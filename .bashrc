#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTCONTROL=ignoreboth:erasedups
export HISTFILE="$XDG_CACHE_HOME/bash_history"

if [ -d "$HOME/.bin" ] ;
	then PATH="$HOME/.bin:$PATH"
fi

[[ -f ~/.aliases ]] && source ~/.aliases

#shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

#create a file called .bashrc-personal and put all your personal aliases
#in there. They will not be overwritten by skel.

[[ -f ~/.bashrc-personal ]] && . ~/.bashrc-personal

neofetch | lolcat

# starship prompt
eval "$(starship init bash)"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/chigozirim/.sdkman"
[[ -s "/home/chigozirim/.sdkman/bin/sdkman-init.sh" ]] && source "/home/chigozirim/.sdkman/bin/sdkman-init.sh"

# added by travis gem
[ -f /home/chigozirim/.travis/travis.sh ] && source /home/chigozirim/.travis/travis.sh
