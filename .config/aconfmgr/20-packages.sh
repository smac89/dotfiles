#!/usr/bin/bash

AddPackage --foreign aconfmgr-git # A configuration manager for Arch Linux
AddPackage --foreign alac-git # a lossless audio codec developed by Apple and deployed on all of its platforms and devices
AddPackage --foreign albert-git # A sophisticated standalone keyboard launcher.
AddPackage --foreign appimagetool-continuous # Package desktop applications as AppImages
AddPackage --foreign apulse # PulseAudio emulation for ALSA
AddPackage --foreign arch-silence-grub-theme-git # Arch Silence - GRUB2 theme - GIT version
AddPackage --foreign archlinux-themes-sddm # Adaptation to SDDM of archlinux-themes-kdm
AddPackage --foreign audio-recorder # Audio Recorder Application
AddPackage --foreign aurutils # helper tools for the arch user repository
AddPackage --foreign autotrace # An utility to trace bitmaps
AddPackage --foreign bibata-extra-cursor-theme # Material Based Cursor Theme
AddPackage --foreign bmap-tools # Tool optimized for copying largely sparse files using information from a block map (bmap) file
AddPackage --foreign brave-bin # Web browser that blocks ads and trackers by default (binary release).
AddPackage --foreign bytecode-viewer # A Java decompiler, editor, debugger, and more
AddPackage --foreign chili-sddm-theme # Chili theme for SDDM
AddPackage --foreign clarity-icon-theme # Vector icons in 9 colourthemes for GTK
AddPackage --foreign code-nautilus-git # VSCode extension for Nautilus
AddPackage --foreign codecs64 # Non-linux native codec pack.
AddPackage --foreign conky-lua-nv # An advanced system monitor for X based on torsmo with lua and nvidia enabled
AddPackage --foreign cpupower-gui # A GUI utility to set CPU frequency limits
AddPackage --foreign docker-credential-secretservice-bin # Store docker credentials using the D-Bus Secret Service
AddPackage --foreign droidcam # A tool for using your android device as a wireless/usb webcam
AddPackage --foreign elementary-xfce-icons # Elementary icon theme with improved Xfce support
AddPackage --foreign faenza-xfce-addon # Add-On package for the faenza Icon Theme to provide better xfce integration
AddPackage --foreign fbrokendesktop # Search for broken Exec in *.desktop
AddPackage --foreign filezilla-bin # Free, open source FTP, FTPS and SFTP client (Pre-built binary)
AddPackage --foreign firefox-nightly # Standalone Web Browser from Mozilla — Nightly build (en-US)
AddPackage --foreign flare-game # Fantasy action RPG using the FLARE engine
AddPackage --foreign flat-remix # Flat Remix is an icon theme inspired by material design. It is mostly flat using a colorful palette with some shadows, highlights, and gradients for some depth.
AddPackage --foreign flat-remix-gtk # Flat Remix GTK theme is a pretty simple gtk window theme inspired on material design following a modern design using "flat" colors with high contrasts and sharp borders.
AddPackage --foreign font-manager # A simple font management application for GTK+ Desktop Environments
AddPackage --foreign genymotion # Complete set of tools that provides a virtual environment for Android.
AddPackage --foreign git-delta-bin # A viewer for git and diff output
AddPackage --foreign glogg # A Qt GUI application to browse and search through long or complex log files.
AddPackage --foreign hblock # An adblocker that creates a hosts file from automatically downloaded sources
AddPackage --foreign indicator-application # Indicator to take menus from applications and place them in the panel
AddPackage --foreign insync # An unofficial Google Drive and OneDrive client that runs on Linux, with support for various desktops
AddPackage --foreign insync-nautilus # Python extension and icons for integrating Insync with Nautilus
AddPackage --foreign javafx-scenebuilder # Gluon Scene Builder for Java 8, based on debian package, this version is recognized by Netbeans, it was released on Jun 5, 2018.
AddPackage --foreign jdk-adoptopenjdk # OpenJDK Java 15 development kit (AdoptOpenJDK build)
AddPackage --foreign jetbrains-toolbox # Manage all your JetBrains Projects and Tools
AddPackage --foreign juno-theme-git # GTK themes inspired by epic vscode themes
AddPackage --foreign kernel-modules-hook # Keeps your system fully functional after a kernel upgrade
AddPackage --foreign lbry-app-bin # The LBRY app built using electron
AddPackage --foreign lbry-sync-ytdl # Sync content to LBRY using youtube-dl
AddPackage --foreign lineageos-devel # Metapackage to pull all dependencies required to build LineageOS
AddPackage --foreign macho # Macho is a man page viewer by HiPhish
AddPackage --foreign mintstick # A GUI to write .img or .iso files to a USB Key. It can also format them
AddPackage --foreign mkinitcpio-openswap # mkinitcpio hook to open swap at boot time
AddPackage --foreign moka-icon-theme-git # An icon theme designed with a minimal, flat style, using simple geometry & colours.
AddPackage --foreign mongodb-compass # The MongoDB GUI
AddPackage --foreign mozregression-gui-bin # Regression range finder for Firefox
AddPackage --foreign nano-syntax-highlighting-git # improved nano syntax highlighting files
AddPackage --foreign nautilus-dropbox # Dropbox for Linux - Nautilus extension
AddPackage --foreign nautilus-ext-git-git # Nautilus extension to add important information about the current git directory
AddPackage --foreign nautilus-open-any-terminal # context-menu entry for opening other terminal in nautilus
AddPackage --foreign nautilus-send-to-bluetooth # Adds 'Send to bluetooth device' to nautilus right-click menu
AddPackage --foreign ncurses5-compat-libs # System V Release 4.0 curses emulation library, ABI 5
AddPackage --foreign networkmanager-dispatcher-openntpd # Dispatcher Script for openntpd
AddPackage --foreign nvm # Node Version Manager - Simple bash script to manage multiple active node.js versions
AddPackage --foreign olive # Free non-linear video editor
AddPackage --foreign paper-icon-theme # Paper is an open source desktop theme and icon project by Sam Hewitt
AddPackage --foreign popcorntime-bin # Stream free movies and TV shows from torrents
AddPackage --foreign postman-bin # Build, test, and document your APIs faster
AddPackage --foreign powershell-bin # A cross-platform automation and configuration tool/framework (binary package)
AddPackage --foreign processing # Programming environment for creating images, animations and interactions
AddPackage --foreign python-injector # Python dependency injection framework, inspired by Guice.
AddPackage --foreign python-py3nvml # Bindings for NVML library.
AddPackage --foreign python2-twodict-git # Simple two way ordered dictionary for Python
AddPackage --foreign qogir-gtk-theme # Qogir is a flat Design theme for GTK
AddPackage --foreign qogir-icon-theme # A colorful design icon theme for linux desktops
AddPackage --foreign qt5-styleplugins # Additional style plugins for Qt5
AddPackage --foreign runescape-launcher # RuneScape Game Client (NXT)
AddPackage --foreign scrcpy # Display and control your Android device
AddPackage --foreign skypeforlinux-stable-bin # Skype for Linux - Stable/Release Version
AddPackage --foreign slack-desktop # Slack Desktop (Beta) for Linux
AddPackage --foreign sound-theme-smooth # Complete system sound theme with 58 event sounds.
AddPackage --foreign srandrd # Simple randr daemon that reacts to monitor hotplug events
AddPackage --foreign starship-bin # The cross-shell prompt for astronauts
AddPackage --foreign sublime-merge # Meet a new Git Client, from the makers of Sublime Text
AddPackage --foreign systemd-removed-services-hook # Notifies you of uninstalled systemd services along with the command to disable them
AddPackage --foreign teamviewer # All-In-One Software for Remote Support and Online Meetings
AddPackage --foreign tela-icon-theme # A flat colorful design icon theme.
AddPackage --foreign telegram-purple # Adds support for Telegram to Pidgin, Adium, Finch and other Libpurple based messengers.
AddPackage --foreign thunar-dropbox # Plugin for Thunar that adds context-menu items for Dropbox.
AddPackage --foreign thunar-vcs-plugin # SVN and GIT integration for Thunar.
AddPackage --foreign tlpui-git # A GTK user interface for TLP written in Python
AddPackage --foreign tp_smapi-dkms # DKMS controlled modules for ThinkPad's SMAPI functionality
AddPackage --foreign ttf-google-fonts-git # TrueType fonts from the Google Fonts project (git version)
AddPackage --foreign ttf-meslo # Meslo LG is a customized version of Apple's Menlo font with various line gap and dotted zero
AddPackage --foreign ttf-muli # Sans serif font by Vernon Adams
AddPackage --foreign ttf-oswald # Sans-serif typeface from Google by Vernon Adams
AddPackage --foreign ttf-quintessential # Calligraphic typeface from Google by Brian J. Bonislawsky
AddPackage --foreign ttf-signika # Sans-serif typeface from Google by Anna Giedryś
AddPackage --foreign ttf-symbola # Font for symbol blocks of the Unicode Standard (TTF)
AddPackage --foreign ttf-twemoji-color # A color and B&W emoji SVG-in-OpenType font by Twitter with support for ZWJ, skin tone modifiers and country flags.
AddPackage --foreign ttf-weather-icons # Icon font with 222 weather themed icons
AddPackage --foreign ttf-wps-fonts # Symbol fonts required by wps-office.
AddPackage --foreign tumbler-extra-thumbnailers # Customized thumbnailers for Tumbler
AddPackage --foreign tzupdate # Set the system timezone based on IP geolocation
AddPackage --foreign udiskie-systemd-git # User systemd service for udiskie auto mounting.
AddPackage --foreign uefitool-git # UEFI firmware image viewer and editor and utilities
AddPackage --foreign ugrep # Universal grep
AddPackage --foreign unbound-root-hints-updater # A systemd timer for updating unbound's root servers.
AddPackage --foreign v4l2ucp # A universal control panel for Video for Linux Two (V4L2) devices
AddPackage --foreign vertex-themes # Vertex Gtk2, Gtk3, Metacity, Xfwm, Cinnamon and GNOME Shell themes (GNOME 3.22 version)
AddPackage --foreign virtualbox-ext-oracle # Oracle VM VirtualBox Extension Pack
AddPackage --foreign visual-studio-code-bin # Visual Studio Code (vscode)
AddPackage --foreign vmware-keymaps # Keymaps required by some VMware packages
AddPackage --foreign vmware-workstation # The industry standard for running multiple operating systems as virtual machines on a single Linux PC.
AddPackage --foreign wpa_supplicant_gui # A Qt frontend for interacting with wpa_supplicant
AddPackage --foreign xdg-utils-mimeo # Command line tools that assist applications with a variety of desktop integration tasks; patched to use mimeo
AddPackage --foreign xfce-theme-greybird # Desktop Suite for Xfce
AddPackage --foreign xfce4-docklike-plugin-git # A modern, docklike, minimalist taskbar for XFCE
AddPackage --foreign xfce4-sensors-plugin-nvidia # A lm_sensors plugin for the Xfce panel with nvidia gpu support
AddPackage --foreign xfce4-statusnotifier-plugin # Plugin to status notifier indicators in the Xfce4 panel
AddPackage --foreign xfdashboard # Maybe a Gnome shell like dashboard for Xfce
AddPackage --foreign xiccd # Simple bridge between colord and X
AddPackage --foreign xscreensaver-arch-logo # Screen saver and locker for the X Window System with Arch Linux branding
AddPackage --foreign yandex-disk # Yandex.Disk keeps your files with you at all times.
AddPackage --foreign yandex-disk-indicator # Panel indicator (GUI) for YandexDisk CLI client for Linux.
AddPackage --foreign zim-git # A WYSIWYG text editor that aims at bringing the concept of a wiki to the desktop. Git Version
AddPackage --foreign zoom # Video Conferencing and Web Conferencing Service


AddPackage accountsservice # D-Bus interface for user account query and manipulation
AddPackage acpi_call-dkms # A linux kernel module that enables calls to ACPI methods through /proc/acpi/call - module sources
AddPackage acpid # A daemon for delivering ACPI power management events with netlink support
AddPackage adapta-gtk-theme # An adaptive Gtk+ theme based on Material Design Guidelines
AddPackage aegisub # A general-purpose subtitle editor with ASS/SSA support
AddPackage alsa-firmware # Firmware binaries for loader programs in alsa-tools and hotplug firmware loader
AddPackage alsa-lib # An alternative implementation of Linux sound support
AddPackage alsa-plugins # Additional ALSA plugins
AddPackage alsa-utils # Advanced Linux Sound Architecture - Utilities
AddPackage ammonite # A cleanroom re-implementation of the Scala REPL from first principles.
AddPackage android-udev # Udev rules to connect Android devices to your linux box
AddPackage apparmor # Mandatory Access Control (MAC) using Linux Security Module (LSM)
AddPackage appimagelauncher # A Helper application for running and integrating AppImages.
AddPackage arc-gtk-theme # A flat theme with transparent elements for GTK 3, GTK 2 and Gnome-Shell
AddPackage arc-icon-theme # Arc icon theme. Official releases only.
AddPackage arc-solid-gtk-theme # A flat theme for GTK 3, GTK 2 and Gnome-Shell (without transparency)
AddPackage arch-install-scripts # Scripts to aid in installing Arch Linux
AddPackage arcolinux-arc-themes-git # Arc themes created for ArcoLinux with Arc Colora script
AddPackage arcolinux-bin-git # .bin folder from arcolinux providing fun scripts
AddPackage arcolinux-config-git # Configuration files for ArcoLinux Iso
AddPackage arcolinux-faces-git # Faces - Avatars created for arcolinux
AddPackage arcolinux-fonts-git # fonts mainly for conkies of arcolinux
AddPackage arcolinux-keyring # ArcoLinux GPG keyring
AddPackage arcolinux-kvantum-git # configuration files for ArcoLinux
AddPackage arcolinux-kvantum-theme-arc-git # kvantum theme for arc for ArcoLinux
AddPackage arcolinux-local-xfce4-git # local for arcolinux
AddPackage arcolinux-mirrorlist-git # ArcoLinux mirrorlist for use by pacman
AddPackage arcolinux-neofetch-git # neofetch config for arcolinux
AddPackage arcolinux-pipemenus-git # Pipemenu's for arcolinux
AddPackage arcolinux-polybar-git # polybar config for arcolinux
AddPackage arcolinux-qt5-git # configuration files for ArcoLinux
AddPackage arcolinux-root-git # root configs from arcolinux
AddPackage arcolinux-system-config-git # Configuration files for ArcoLinux
AddPackage arcolinux-tweak-tool-git # arcolinux tweak tool
AddPackage arcolinux-welcome-app-git # Welcome application for arcolinux
AddPackage arcolinux-xfce-git # Desktop configuration for ArcoLinux -D -B
AddPackage arcolinux-xfce4-panel-profiles-git # ArcoLinux Xfce Panel Profile for ArcoLinux
AddPackage arduino # Arduino prototyping platform SDK
AddPackage arduino-docs # Arduino IDE reference documentation
AddPackage aria2 # Download utility that supports HTTP(S), FTP, BitTorrent, and Metalink
AddPackage asciinema # Record and share terminal sessions
AddPackage autoconf # A GNU tool for automatically configuring source code
AddPackage automake # A GNU tool for automatically creating Makefiles
AddPackage avahi # Service Discovery for Linux using mDNS/DNS-SD -- compatible with Bonjour
AddPackage awesome-terminal-fonts # fonts/icons for powerlines
AddPackage baobab # A graphical directory tree analyzer
AddPackage bash-completion # Programmable completion for the bash shell
AddPackage bibata-cursor-theme # Material Based Cursor Theme
AddPackage bind # A complete, highly portable implementation of the DNS protocol
AddPackage binutils # A set of programs to assemble and manipulate binary and object files
AddPackage bison # The GNU general-purpose parser generator
AddPackage blender # A fully integrated 3D graphics creation suite
AddPackage blueberry # Bluetooth configuration tool
AddPackage blueman # GTK+ Bluetooth Manager
AddPackage bluez # Daemons for the bluetooth protocol stack
AddPackage bluez-libs # Deprecated libraries for the bluetooth protocol stack
AddPackage bluez-utils # Development and debugging utilities for the bluetooth protocol stack
AddPackage bookworm # A simple user centric eBook reader which displays multiple eBooks formats uniformly
AddPackage boost # Free peer-reviewed portable C++ source libraries - development headers
AddPackage boost-libs # Free peer-reviewed portable C++ source libraries - runtime libraries
AddPackage breeze-gtk # Breeze widget theme for GTK 2 and 3
AddPackage broadcom-wl-dkms # Broadcom 802.11 Linux STA wireless driver
AddPackage btrfs-progs # Btrfs filesystem utilities
AddPackage buildah # A tool which facilitates building OCI images
AddPackage busybox # Utilities for rescue and embedded systems
AddPackage caffeine-ng # Status bar application able to temporarily inhibit the screensaver and sleep mode.
AddPackage catfish # Versatile file searching tool
AddPackage ccid # A generic USB Chip/Smart Card Interface Devices driver
AddPackage chntpw # Offline NT Password Editor - reset passwords in a Windows NT SAM user database file
AddPackage chromium-bsu # A fast paced top scrolling shooter
AddPackage cin # Cinelerra git
AddPackage cloc # Count lines of code
AddPackage clonezilla # ncurses partition and disk imaging/cloning program
AddPackage colordiff # A Perl script wrapper for 'diff' that produces the same output but with pretty 'syntax' highlighting
AddPackage coqide # GTK-based graphical interface for the Coq proof assistant
AddPackage cpupower # Linux kernel tool to examine and tune power saving related features of your processor
AddPackage crda # Central Regulatory Domain Agent for wireless networks
AddPackage cronie # Daemon that runs specified programs at scheduled times and related tools
AddPackage cryptsetup # Userspace setup tool for transparent encryption of block devices using dm-crypt
AddPackage ctags # Generates an index file of language objects found in source files
AddPackage cups # The CUPS Printing System - daemon package
AddPackage cups-pdf # PDF printer for cups
AddPackage cups-pk-helper # A helper that makes system-config-printer use PolicyKit
AddPackage d-feet # D-Bus debugger for GNOME
AddPackage darkhttpd # A small and secure static webserver
AddPackage dbus # Freedesktop.org message bus system
AddPackage dbus-broker # Linux D-Bus Message Broker
AddPackage dconf-editor # dconf Editor
AddPackage ddrescue # GNU data recovery tool
AddPackage deno # A secure runtime for JavaScript and TypeScript
AddPackage device-mapper # Device mapper userspace library and tools
AddPackage dex # Program to generate and execute DesktopEntry files of type Application
AddPackage dhclient # A standalone DHCP client from the dhcp package
AddPackage diffuse # Graphical tool for merging and comparing text files
AddPackage diffutils # Utility programs used for creating patch files
AddPackage discord # All-in-one voice and text chat for gamers that's free and secure.
AddPackage displaycal # Open Source Display Calibration and Characterization powered by Argyll CMS (Formerly known as dispcalGUI)
AddPackage dmenu # Generic menu for X
AddPackage dmidecode # Desktop Management Interface table related utilities
AddPackage dmraid # Device mapper RAID interface
AddPackage dnsmasq # Lightweight, easy to configure DNS forwarder and DHCP server
AddPackage dosfstools # DOS filesystem utilities
AddPackage downgrade # Bash script for downgrading one or more packages to a version in your cache or the A.L.A.
AddPackage dpkg # The Debian Package Manager tools
AddPackage efitools # Tools for manipulating UEFI secure boot platforms
AddPackage emacs # The extensible, customizable, self-documenting real-time display editor
AddPackage eog # Eye of Gnome
AddPackage eog-plugins # Plugins for Eye of Gnome
AddPackage ethtool # Utility for controlling network drivers and hardware
AddPackage evince # Document viewer (PDF, PostScript, XPS, djvu, dvi, tiff, cbr, cbz, cb7, cbt)
AddPackage evolution # Manage your email, contacts and schedule
AddPackage evolution-ews # MS Exchange integration through Exchange Web Services
AddPackage exfat-utils # Utilities for exFAT file system
AddPackage exo # Application library for Xfce
AddPackage expac # pacman database extraction utility
AddPackage f2fs-tools # Tools for Flash-Friendly File System (F2FS)
AddPackage faenza-icon-theme # Icon theme designed for Equinox GTK theme
AddPackage fakeroot # Tool for simulating superuser privileges
AddPackage feh # Fast and light imlib2-based image viewer
AddPackage ffmpegthumbnailer # Lightweight video thumbnailer that can be used by file managers.
AddPackage file # File type identification utility
AddPackage file-roller # Create and modify archives
AddPackage filemanager-actions # File-manager extension which offers user configurable context menu actions
AddPackage findutils # GNU utilities to locate files
AddPackage flatbuffers # An efficient cross platform serialization library for C++, with support for Java, C# and Go
AddPackage flex # A tool for generating text-scanning programs
AddPackage foomatic-db-gutenprint-ppds # simplified prebuilt ppd files
AddPackage freerdp # Free implementation of the Remote Desktop Protocol (RDP)
AddPackage freetype2 # Font rasterization library
AddPackage fsarchiver # Safe and flexible file-system backup and deployment tool
AddPackage fwbuilder # Object-oriented GUI and set of compilers for various firewall platforms
AddPackage garcon # Implementation of the freedesktop.org menu specification
AddPackage gawk # GNU version of awk
AddPackage gcc # The GNU Compiler Collection - C and C++ frontends
AddPackage gdb # The GNU Debugger
AddPackage gettext # GNU internationalization library
AddPackage ghostscript # An interpreter for the PostScript language
AddPackage ghostwriter # Aesthetic, distraction-free Markdown editor
AddPackage gifski # GIF encoder based on libimagequant (pngquant). Squeezes maximum possible quality from the awful GIF format.
AddPackage gimp # GNU Image Manipulation Program
AddPackage git # the fast distributed version control system
AddPackage git-lfs # Git extension for versioning large files
AddPackage github-cli # The GitHub CLI
AddPackage gksu # A graphical frontend for su
AddPackage gnome-books # Access and organize your e-books on GNOME
AddPackage gnome-boxes # Simple GNOME application to access remote or virtual systems
AddPackage gnome-calculator # GNOME Scientific calculator
AddPackage gnome-calendar # Simple and beautiful calendar application designed to perfectly fit the GNOME desktop
AddPackage gnome-common # Common development macros for GNOME
AddPackage gnome-icon-theme # GNOME icon theme
AddPackage gnome-keyring # Stores passwords and encryption keys
AddPackage gnome-maps # A simple GNOME 3 maps application
AddPackage gnome-screenshot # Take pictures of your screen
AddPackage gnome-user-docs # User documentation for GNOME
AddPackage gnu-netcat # GNU rewrite of netcat, the network piping application
AddPackage gobject-introspection # Introspection system for GObject-based libraries
AddPackage gparted # A Partition Magic clone, frontend to GNU Parted
AddPackage gpick # Advanced color picker written in C++ using GTK+ toolkit
AddPackage gpm # A mouse server for the console and xterm
AddPackage gptfdisk # A text-mode partitioning tool that works on GUID Partition Table (GPT) disks
AddPackage grep # A string search utility
AddPackage grsync # GTK+ GUI for rsync to synchronize folders, files and make backups
AddPackage gsfonts # (URW)++ Core Font Set [Level 2]
AddPackage gst-libav # Multimedia graph framework - libav plugin
AddPackage gst-plugin-gtk # Multimedia graph framework - gtk plugin
AddPackage gst-plugins-bad # Multimedia graph framework - bad plugins
AddPackage gst-plugins-base # Multimedia graph framework - base plugins
AddPackage gst-plugins-good # Multimedia graph framework - good plugins
AddPackage gst-plugins-ugly # Multimedia graph framework - ugly plugins
AddPackage gstreamer # Multimedia graph framework - core
AddPackage gtk-engine-murrine # GTK2 engine to make your desktop look like a 'murrina', an italian word meaning the art glass works done by Venicians glass blowers.
AddPackage gtk3 # GObject-based multi-platform GUI toolkit
AddPackage gufw # Uncomplicated way to manage your Linux firewall
AddPackage gutenprint # Top quality printer drivers for POSIX systems
AddPackage guvcview # Simple GTK+ interface for capturing and viewing video from v4l2 devices
AddPackage gvfs # Virtual filesystem implementation for GIO
AddPackage gvfs-afc # Virtual filesystem implementation for GIO (AFC backend; Apple mobile devices)
AddPackage gvfs-gphoto2 # Virtual filesystem implementation for GIO (gphoto2 backend; PTP camera, MTP media player)
AddPackage gvfs-mtp # Virtual filesystem implementation for GIO (MTP backend; Android, media player)
AddPackage gvfs-nfs # Virtual filesystem implementation for GIO (NFS backend)
AddPackage gvfs-smb # Virtual filesystem implementation for GIO (SMB/CIFS backend; Windows client)
AddPackage gzip # GNU compression utility
AddPackage halo-icons-git # Halo is an icon theme for Linux desktops, the set is inspired by the latest flat design trend from Alejandro Camarena
AddPackage hardcode-fixer-git # Fixes Hardcoded Icons
AddPackage hardinfo # A system information and benchmark tool.
AddPackage hddtemp # Gives you the temperature of your hard drive by reading S.M.A.R.T. information
AddPackage hdparm # A shell utility for manipulating Linux IDE drive/driver parameters
AddPackage htop # Interactive process viewer
AddPackage httpie # cURL for humans
AddPackage hunspell-en_CA # CA English hunspell dictionaries
AddPackage hunspell-en_US # US English hunspell dictionaries
AddPackage hw-probe # Tool to probe for hardware, check its operability and upload result to the Linux hardware DB
AddPackage hwinfo # Hardware detection tool from openSUSE
AddPackage i7z # A better i7 (and now i3, i5) reporting tool for Linux
AddPackage imagemagick # An image viewing/manipulation program
AddPackage inetutils # A collection of common network programs
AddPackage inkscape # Professional vector graphics editor
AddPackage inotify-tools # inotify-tools is a C library and a set of command-line programs for Linux providing a simple interface to inotify.
AddPackage intel-ucode # Microcode update files for Intel CPUs
AddPackage intel-undervolt # Intel CPU undervolting tool
AddPackage intltool # The internationalization tool collection
AddPackage inxi # Full featured CLI system information tool
AddPackage iproute2 # IP Routing Utilities
AddPackage iputils # Network monitoring tools, including ping
AddPackage ipw2100-fw # Intel Centrino Drivers firmware for IPW2100
AddPackage ipw2200-fw # Firmware for the Intel PRO/Wireless 2200BG
AddPackage irssi # Modular text mode IRC client with Perl scripting
AddPackage iw # nl80211 based CLI configuration utility for wireless devices
AddPackage jack2 # C++ version of the JACK low-latency audio server for multi-processor machines
AddPackage jdk-openjdk # OpenJDK Java 14 development kit
AddPackage jdk11-openjdk # OpenJDK Java 11 development kit
AddPackage jdk8-openjdk # OpenJDK Java 8 development kit
AddPackage jfsutils # JFS filesystem utilities
AddPackage jgmenu # Simple, independent, contemporary-looking X11 menu, designed for scripting, ricing and tweaking
AddPackage jq # Command-line JSON processor
AddPackage jsoncpp # C++ library for interacting with JSON
AddPackage julia # High-level, high-performance, dynamic programming language
AddPackage kcoreaddons # Addons to QtCore
AddPackage kde-cli-tools # Tools based on KDE Frameworks 5 to better interact with the system
AddPackage kdeconnect # Adds communication between KDE and your smartphone
AddPackage klavaro # Free touch typing tutor program
AddPackage kleopatra # Certificate Manager and Unified Crypto GUI
AddPackage kmag # Screen Magnifier
AddPackage kshutdown # Shutdown Utility
AddPackage ktimer # Countdown Launcher
AddPackage kvantum-qt5 # SVG-based theme engine for Qt5 (including config tool and extra themes)
AddPackage kvantum-theme-materia # Materia theme for KDE Plasma 5
AddPackage laptop-detect # Attempts to detect a laptop
AddPackage less # A terminal based program for viewing text files
AddPackage lib32-fakeroot # Tool for simulating superuser privileges (32-bit)
AddPackage lib32-gcc-libs # 32-bit runtime libraries shipped by GCC
AddPackage lib32-libltdl # A generic library support script (32-bit)
AddPackage libcups # The CUPS Printing System - client libraries and headers
AddPackage libdvdcss # Portable abstraction library for DVD decryption
AddPackage libfaketime # Report fake dates and times to programs without having to change the system-wide time.
AddPackage libgsf # An extensible I/O abstraction library for dealing with structured file formats
AddPackage libmtp # Library implementation of the Media Transfer Protocol
AddPackage libnma # NetworkManager GUI client library
AddPackage libopenraw # Library for decoding RAW files
AddPackage libpam-google-authenticator # PAM module for google authenticator app
AddPackage libpwquality # Library for password quality checking and generating random passwords
AddPackage libreoffice-fresh # LibreOffice branch which contains new features and program enhancements
AddPackage libsass # C implementation of Sass CSS preprocessor (library).
AddPackage libtool # A generic library support script
AddPackage libva-intel-driver # VA-API implementation for Intel G45 and HD Graphics family
AddPackage libva-mesa-driver # VA-API implementation for gallium
AddPackage libvncserver # Cross-platform C libraries that allow you to easily implement VNC server or client functionality
AddPackage libxfce4ui # Commonly used Xfce widgets among Xfce applications
AddPackage libxfce4util # Basic utility non-GUI functions for Xfce
AddPackage licenses # Standard licenses distribution package
AddPackage lifeograph # Private journal, diary and note taking application
AddPackage light # Program to easily change brightness on backlight-controllers.
AddPackage linux # The Linux kernel and modules
AddPackage linux-firmware # Firmware files for Linux
AddPackage linux-headers # Headers and scripts for building modules for the Linux kernel
AddPackage linux-lts # The LTS Linux kernel and modules
AddPackage linux-lts-headers # Headers and scripts for building modules for the LTS Linux kernel
AddPackage lm_sensors # Collection of user space tools for general SMBus access and hardware monitoring
AddPackage logrotate # Rotates system logs automatically
AddPackage lolcat # Okay, no unicorns. But rainbows!!
AddPackage lostfiles # Find orphaned files not owned by any Arch packages
AddPackage lsb-release # LSB version query program
AddPackage lshw # A small tool to provide detailed information on the hardware configuration of the machine.
AddPackage lsof # Lists open files for running Unix processes
AddPackage lsscsi # A tool that lists devices connected via SCSI and its transports
AddPackage luit # Filter that can be run between an arbitrary application and a UTF-8 terminal emulator
AddPackage lvm2 # Logical Volume Manager 2 utilities
AddPackage m4 # The GNU macro processor
AddPackage make # GNU make utility to maintain groups of programs
AddPackage man-db # A utility for reading man pages
AddPackage man-pages # Linux man pages
AddPackage mdadm # A tool for managing/monitoring Linux md device arrays, also known as Software RAID
AddPackage meld # Compare files, directories and working copies
AddPackage memtest86+ # An advanced memory diagnostic tool
AddPackage menulibre # An advanced menu editor that provides modern features in a clean, easy-to-use interface
AddPackage mercurial # A scalable distributed SCM tool
AddPackage mesa # An open-source implementation of the OpenGL specification
AddPackage meson # High productivity build system
AddPackage mimeo # Open files by MIME-type or file name using regular expressions.
AddPackage mkinitcpio-nfs-utils # ipconfig and nfsmount tools for NFS root support in mkinitcpio
AddPackage mlocate # Merging locate/updatedb implementation
AddPackage mobile-broadband-provider-info # Network Management daemon
AddPackage modemmanager # Mobile broadband modem management service
AddPackage moreutils # A growing collection of the unix tools that nobody thought to write thirty years ago
AddPackage mosh # Mobile shell, surviving disconnects with local echo and line editing
AddPackage mtools # A collection of utilities to access MS-DOS disks
AddPackage mtpfs # A FUSE filesystem that supports reading and writing from any MTP device
AddPackage mugshot # Program to update personal user details
AddPackage muparser # A fast math parser library
AddPackage namcap # A Pacman package analyzer
AddPackage nano # Pico editor clone with enhancements
AddPackage nautilus # Default file manager for GNOME
AddPackage nautilus-image-converter # Nautilus extension to rotate/resize image files
AddPackage nbd # tools for network block devices, allowing you to use remote block devices over TCP/IP
AddPackage ndisc6 # Collection of IPv6 networking utilities
AddPackage neofetch # A CLI system information tool written in BASH that supports displaying images.
AddPackage netpbm # A toolkit for manipulation of graphic images
AddPackage network-manager-applet # Applet for managing network connections
AddPackage networkmanager # Network connection manager and user applications
AddPackage networkmanager-openconnect # NetworkManager VPN plugin for OpenConnect
AddPackage networkmanager-openvpn # NetworkManager VPN plugin for OpenVPN
AddPackage networkmanager-pptp # NetworkManager VPN plugin for PPTP
AddPackage networkmanager-vpnc # NetworkManager VPN plugin for VPNC
AddPackage nextcloud-client # Nextcloud desktop client
AddPackage nfs-utils # Support programs for Network File Systems
AddPackage nilfs-utils # A log-structured file system supporting continuous snapshotting (userspace utils)
AddPackage nm-connection-editor # NetworkManager GUI connection editor and widgets
AddPackage nomacs # A Qt image viewer
AddPackage noto-fonts # Google Noto TTF fonts
AddPackage noto-fonts-cjk # Google Noto CJK fonts
AddPackage noto-fonts-emoji # Google Noto emoji fonts
AddPackage noto-fonts-extra # Google Noto TTF fonts - additional variants
AddPackage nss-mdns # glibc plugin providing host name resolution via mDNS
AddPackage ntfs-3g # NTFS filesystem driver and utilities
AddPackage nuitka # Python compiler with full language support and CPython compatibility
AddPackage numix-circle-arc-icons-git # Install Numix and Numix Circle. Numix icons combined with the Arc folders
AddPackage numix-circle-icon-theme-git # Circle icon theme from the Numix project
AddPackage numix-icon-theme-git # Base icon theme from the Numix project
AddPackage numlockx # Turns on the numlock key in X11.
AddPackage nvidia-dkms # NVIDIA drivers - module sources
AddPackage nvidia-prime # NVIDIA Prime Render Offload configuration and utilities
AddPackage nvidia-settings # Tool for configuring the NVIDIA graphics driver
AddPackage opam # OCaml package manager
AddPackage openapi-generator # Generation of API client libraries, server stubs, documentation and configuration
AddPackage openbox-themes-pambudi-git # Openbox themes from Adhi Pambudi
AddPackage opencl-headers # OpenCL (Open Computing Language) header files
AddPackage opencl-nvidia # OpenCL implemention for NVIDIA
AddPackage openconnect # Open client for Cisco AnyConnect VPN
AddPackage openjdk-src # OpenJDK Java 14 sources
AddPackage openjdk11-src # OpenJDK Java 11 sources
AddPackage openjdk8-src # OpenJDK Java 8 sources
AddPackage openmpi # High performance message passing library (MPI)
AddPackage openntpd # Free, easy to use implementation of the Network Time Protocol
AddPackage opensc # Tools and libraries for smart cards
AddPackage openssh # Premier connectivity tool for remote login with the SSH protocol
AddPackage openvpn # An easy-to-use, robust and highly configurable VPN (Virtual Private Network)
AddPackage optimus-manager # Management utility to handle GPU switching for Optimus laptops
AddPackage optimus-manager-qt # A Qt interface for Optimus Manager that allows to configure and switch GPUs on Optimus laptops using the tray menu
AddPackage oxy-neon # Stylized oxygen mouse theme created for use with dark desktop and especially for FRUiT's Neon suite.
AddPackage p7zip # Command-line file archiver with high compression ratio
AddPackage pacman # A library-based package manager with dependency support
AddPackage pacutils # Helper tools for libalpm
AddPackage papirus-icon-theme # Papirus icon theme
AddPackage paprefs # Configuration dialog for PulseAudio
AddPackage parallel # A shell tool for executing jobs in parallel
AddPackage partclone # Utilities to save and restore used blocks on a partition
AddPackage parted # A program for creating, destroying, resizing, checking and copying partitions
AddPackage partimage # Partition Image saves partitions in many formats to an image file.
AddPackage patch # A utility to apply patch files to original sources
AddPackage pavucontrol # PulseAudio Volume Control
AddPackage pciutils # PCI bus configuration space access library and tools
AddPackage pdftk # Command-line tool for working with PDFs
AddPackage peek # Simple screen recorder with an easy to use interface
AddPackage phonon-qt5 # The multimedia framework by KDE
AddPackage pidgin # Multi-protocol instant messaging client
AddPackage pkgconf # Package compiler and linker metadata toolkit
AddPackage pkgfile # a pacman .files metadata explorer
AddPackage playerctl # mpris media player controller and lib for spotify, vlc, audacious, bmp, xmms2, and others.
AddPackage podman # Tool and library for running OCI-based containers in pods
AddPackage podman-docker # Emulate Docker CLI using podman
AddPackage polkit # Application development toolkit for controlling system-wide privileges
AddPackage polkit-gnome # Legacy polkit authentication agent for GNOME
AddPackage polybar # A fast and easy-to-use status bar
AddPackage poppler-glib # Poppler glib bindings
AddPackage poppler-qt5 # Poppler Qt5 bindings
AddPackage postgresql # Sophisticated object-relational DBMS
AddPackage powerpill # Pacman wrapper for faster downloads.
AddPackage powertop # A tool to diagnose issues with power consumption and power management
AddPackage ppp # A daemon which implements the Point-to-Point Protocol for dial-up networking
AddPackage pptpclient # Client for the proprietary Microsoft Point-to-Point Tunneling Protocol, PPTP.
AddPackage pragha # A lightweight GTK+ music manager - fork of Consonance Music Manager.
AddPackage procps-ng # Utilities for monitoring your system and its processes
AddPackage profile-sync-daemon # Symlinks and syncs browser profile dirs to RAM
AddPackage psiconv # Converts Psion 5(MX) files to more commonly used file formats
AddPackage psmisc # Miscellaneous procfs tools
AddPackage pulseaudio # A featureful, general-purpose sound server
AddPackage pulseaudio-alsa # ALSA Configuration for PulseAudio
AddPackage pulseaudio-bluetooth # Bluetooth support for PulseAudio
AddPackage pulseaudio-zeroconf # Zeroconf support for PulseAudio
AddPackage python-cx-freeze # Create standalone executables from Python scripts
AddPackage python-docutils # Set of tools for processing plaintext docs into formats such as HTML, XML, or LaTeX
AddPackage python-gdal # Python bindings for GDAL
AddPackage python-nautilus # Python bindings for the Nautilus Extension API
AddPackage python-owslib # Python package for client programming with Open Geospatial Consortium (OGC) web service interface standards, and their related content models
AddPackage python-pip # The PyPA recommended tool for installing Python packages
AddPackage python-pipenv # Sacred Marriage of Pipfile, Pip, & Virtualenv.
AddPackage python-protobuf # Python 3 bindings for Google Protocol Buffers
AddPackage python-psycopg2 # A PostgreSQL database adapter for the Python programming language.
AddPackage python-pywal # Generate and change colorschemes on the fly
AddPackage python-rx # Reactive Extensions for Python
AddPackage python-twisted # Asynchronous networking framework written in Python
AddPackage python2-pyxdg # Python library to access freedesktop.org standards
AddPackage qalculate-gtk # GTK frontend for libqalculate
AddPackage qbittorrent # An advanced BitTorrent client programmed in C++, based on Qt toolkit and libtorrent-rasterbar.
AddPackage qgis # Geographic Information System (GIS) that supports vector, raster & database formats
AddPackage qjournalctl # Qt-based graphical user interface for systemd's journalctl command
AddPackage qt5-doc # A cross-platform application and UI framework (Documentation)
AddPackage qt5-tools # A cross-platform application and UI framework (Development Tools, QtHelp)
AddPackage qt5-webkit # Classes for a WebKit2 based implementation and a new QML API
AddPackage qt5-websockets # Provides WebSocket communication compliant with RFC 6455
AddPackage qt5-xmlpatterns # Support for XPath, XQuery, XSLT and XML schema validation
AddPackage qt5ct # Qt5 Configuration Utility
AddPackage qtcreator # Lightweight, cross-platform integrated development environment
AddPackage rdesktop # An open source client for Windows Remote Desktop Services
AddPackage realtime-privileges # Realtime privileges for users
AddPackage redshift # Adjusts the color temperature of your screen according to your surroundings.
AddPackage refind # An EFI boot manager
AddPackage reflector # A Python 3 module and script to retrieve and filter the latest Pacman mirror list.
AddPackage reiserfsprogs # Reiserfs utilities
AddPackage remmina # remote desktop client written in GTK+
AddPackage ristretto # Fast and lightweight picture-viewer for Xfce4
AddPackage rofi # A window switcher, application launcher and dmenu replacement
AddPackage rp-pppoe # Roaring Penguin's Point-to-Point Protocol over Ethernet client
AddPackage rrdtool # Data logging and graphing application
AddPackage rsync # A fast and versatile file copying tool for remote and local files
AddPackage rust # Systems programming language focused on safety, speed and concurrency
AddPackage rygel # UPnP AV MediaServer and MediaRenderer that allows you to easily share audio, video and pictures, and control of media player on your home network
AddPackage s-nail # Environment for sending and receiving mail
AddPackage s-tui # Terminal UI stress test and monitoring tool
AddPackage sardi-icons # Sardi is an icon collection for any linux distro with 6 different circular icons and 10 different kind of folders.
AddPackage sassc # C implementation of Sass CSS preprocessor.
AddPackage screenkey-git # Screencast tool to show your keys inspired by Screenflick, based on key-mon. Active fork with new features.
AddPackage scrot # Simple command-line screenshot utility for X
AddPackage sddm # QML based X11 and Wayland display manager
AddPackage sddm-config-editor-git # SDDM Configuration Editor
AddPackage sdparm # An utility similar to hdparm but for SCSI devices
AddPackage seahorse # GNOME application for managing PGP keys.
AddPackage seahorse-nautilus # PGP encryption and signing for nautilus
AddPackage sed # GNU stream editor
AddPackage sg3_utils # Generic SCSI utilities
AddPackage simple-scan # Simple scanning utility
AddPackage simplescreenrecorder # A feature-rich screen recorder that supports X11 and OpenGL.
AddPackage slop # Utility to query the user for a selection and print the region to stdout
AddPackage smartmontools # Control and monitor S.M.A.R.T. enabled ATA and SCSI Hard Drives
AddPackage sof-firmware # Sound Open Firmware
AddPackage speedtest-cli # Command line interface for testing internet bandwidth using speedtest.net
AddPackage splix # CUPS drivers for SPL (Samsung Printer Language) printers
AddPackage squashfs-tools # Tools for squashfs, a highly compressed read-only filesystem for Linux.
AddPackage strace # A diagnostic, debugging and instructional userspace tracer
AddPackage strongswan # Open source IPsec implementation
AddPackage sublime-text-dev # Sophisticated text editor for code, html and prose - dev build
AddPackage subversion # A Modern Concurrent Version Control System
AddPackage sudo # Give certain users the ability to run some commands as root
AddPackage supertux # A classic 2D jump'n'run sidescroller game in a style similar to the original SuperMario games
AddPackage surfn-icons-git # Surfn is a colourful icon theme.
AddPackage swig # Generate scripting interfaces to C/C++ code
AddPackage sysfsutils # System Utilities Based on Sysfs
AddPackage syslinux # Collection of boot loaders that boot from FAT, ext2/3/4 and btrfs filesystems, from CDs and via PXE
AddPackage syslog-ng # Next-generation syslogd with advanced networking and filtering capabilities
AddPackage system-config-printer # A CUPS printer configuration tool and status applet
AddPackage systemd-sysvcompat # sysvinit compat for systemd
AddPackage systemdgenie # Systemd management utility
AddPackage tar # Utility used to store, backup, and transport files
AddPackage tcpdump # Powerful command-line packet analyzer
AddPackage testdisk # Checks and undeletes partitions + PhotoRec, signature based recovery tool
AddPackage testssl.sh # Testing TLS/SSL encryption
AddPackage texinfo # GNU documentation system for on-line information and printed output
AddPackage thunar # Modern file manager for Xfce
AddPackage thunar-archive-plugin # Create and extract archives in Thunar
AddPackage thunar-media-tags-plugin # Adds special features for media files to the Thunar File Manager
AddPackage thunar-volman # Automatic management of removeable devices in Thunar
AddPackage thunderbird # Standalone mail and news reader from mozilla.org
AddPackage thunderbird-extension-enigmail # OpenPGP message encryption and authentication for Thunderbird
AddPackage timeshift # A system restore utility for Linux
AddPackage tlp # Linux Advanced Power Management
AddPackage tlp-rdw # Linux Advanced Power Management - Radio Device Wizard
AddPackage totem # Movie player for the GNOME desktop based on GStreamer
AddPackage traceroute # Tracks the route taken by packets over an IP network
AddPackage trash-cli # Command line trashcan (recycle bin) interface
AddPackage tree # A directory listing program displaying a depth indented list of files
AddPackage ttf-bitstream-vera # Bitstream Vera fonts.
AddPackage ttf-dejavu # Font family based on the Bitstream Vera Fonts with a wider range of characters
AddPackage ttf-droid # General-purpose fonts released by Google as part of Android
AddPackage ttf-font-awesome # Iconic font designed for Bootstrap
AddPackage ttf-hack # A hand groomed and optically balanced typeface based on Bitstream Vera Mono.
AddPackage ttf-liberation # Red Hats Liberation fonts
AddPackage ttf-ms-fonts # Core TTF Fonts from Microsoft
AddPackage ttf-ubuntu-font-family # Ubuntu font family
AddPackage tumbler # D-Bus service for applications to request thumbnails
AddPackage udiskie # Removable disk automounter using udisks
AddPackage udisks2 # Disk Management Service, version 2
AddPackage ufw # Uncomplicated and easy to use CLI tool for managing a netfilter firewall
AddPackage uget # GTK+ download manager featuring download classification and HTML import
AddPackage unace # An extraction tool for the proprietary ace archive format
AddPackage unbound # Validating, recursive, and caching DNS resolver
AddPackage uncrustify # A source code beautifier
AddPackage unrar # The RAR uncompression program
AddPackage unzip # For extracting and viewing files in .zip archives
AddPackage upower # Abstraction for enumerating power devices, listening to device events and querying history and statistics
AddPackage usb_modeswitch # Activating switchable USB devices on Linux.
AddPackage usbutils # A collection of USB tools to query connected USB devices
AddPackage valgrind # Tool to help find memory-management problems in programs
AddPackage variety # Changes the wallpaper on a regular interval using user-specified or automatically downloaded images.
AddPackage ventoy-bin # A new multiboot USB solution (Binary)
AddPackage vim # Vi Improved, a highly configurable, improved version of the vi text editor
AddPackage virtualbox # Powerful x86 virtualization for enterprise as well as home use
AddPackage virtualbox-host-dkms # VirtualBox Host kernel modules sources
AddPackage visualvm # Visual tool integrating several commandline JDK tools and lightweight profiling capabilities
AddPackage vlc # Multi-platform MPEG, VCD/DVD, and DivX player
AddPackage vnstat # A console-based network traffic monitor
AddPackage vpnc # VPN client for cisco3000 VPN Concentrators
AddPackage vulkan-intel # Intel's Vulkan mesa driver
AddPackage webkit2gtk # GTK+ Web content engine library
AddPackage wget # Network utility to retrieve files from the Web
AddPackage which # A utility to show the full path of commands
AddPackage wireless_tools # Tools allowing to manipulate the Wireless Extensions
AddPackage wireshark-qt # Network traffic and protocol analyzer/sniffer - Qt GUI
AddPackage wmctrl # Control your EWMH compliant window manager from command line
AddPackage wpa_supplicant # A utility providing key negotiation for WPA wireless networks
AddPackage wttr # a simple script that helps you check weather condition using site 
AddPackage x86_energy_perf_policy # Read or write MSR_IA32_ENERGY_PERF_BIAS
AddPackage xapp # Common library for X-Apps projectfile
AddPackage xcursor-simpleandsoft # A simple and soft X cursor theme
AddPackage xcursor-vanilla-dmz-aa # Vanilla DMZ AA cursor theme
AddPackage xdg-user-dirs # Manage user directories like ~/Desktop and ~/Music
AddPackage xdotool # Command-line X11 automation tool
AddPackage xf86-input-elographics # X.org Elographics TouchScreen input driver
AddPackage xf86-input-evdev # X.org evdev input driver
AddPackage xf86-input-libinput # Generic input driver for the X.Org server based on libinput
AddPackage xf86-input-vmmouse # X.org VMWare Mouse input driver
AddPackage xf86-input-void # X.org void input driver
AddPackage xf86-video-amdgpu # X.org amdgpu video driver
AddPackage xf86-video-ati # X.org ati video driver
AddPackage xf86-video-fbdev # X.org framebuffer video driver
AddPackage xf86-video-openchrome # X.Org Openchrome drivers
AddPackage xf86-video-vesa # X.org vesa video driver
AddPackage xf86-video-vmware # X.org vmware video driver
AddPackage xfce4-appfinder # An application finder for Xfce
AddPackage xfce4-battery-plugin # A battery monitor plugin for the Xfce panel
AddPackage xfce4-clipman-plugin # A clipboard plugin for the Xfce4 panel
AddPackage xfce4-cpufreq-plugin # CPU frequency plugin for the Xfce4 panel
AddPackage xfce4-cpugraph-plugin # CPU graph plugin for the Xfce4 panel
AddPackage xfce4-datetime-plugin # A date and time display plugin for the Xfce panel
AddPackage xfce4-dict # A dictionary plugin for the Xfce panel
AddPackage xfce4-diskperf-plugin # Plugin for the Xfce4 panel displaying instant disk/partition performance
AddPackage xfce4-eyes-plugin # A rolling eyes (following mouse pointer) plugin for the Xfce panel
AddPackage xfce4-fsguard-plugin # File system usage monitor plugin for the Xfce4 panel
AddPackage xfce4-genmon-plugin # plugin that monitors customizable programs stdout for the Xfce4 panel
AddPackage xfce4-mailwatch-plugin # Multi-protocol, multi-mailbox mail watcher for the Xfce4 panel
AddPackage xfce4-mount-plugin # Mount/umount utility for the Xfce4 panel
AddPackage xfce4-mpc-plugin # Control the Music Player Daemon from the Xfce4 panel
AddPackage xfce4-netload-plugin # A netload plugin for the Xfce panel
AddPackage xfce4-notes-plugin # A notes plugin for the Xfce4 panel
AddPackage xfce4-notifyd # Notification daemon for the Xfce desktop
AddPackage xfce4-panel # Panel for the Xfce desktop environment
AddPackage xfce4-panel-profiles # Simple application to manage Xfce panel layouts
AddPackage xfce4-power-manager # Power manager for Xfce desktop
AddPackage xfce4-pulseaudio-plugin # Pulseaudio plugin for Xfce4 panel
AddPackage xfce4-screenshooter # Plugin that makes screenshots for the Xfce panel
AddPackage xfce4-session # A session manager for Xfce
AddPackage xfce4-settings # Settings manager of the Xfce desktop
AddPackage xfce4-smartbookmark-plugin # Plugin for the Xfce4 panel that lets you quicksearch from selected websites
AddPackage xfce4-systemload-plugin # A system load plugin for the Xfce4 panel
AddPackage xfce4-taskmanager # Easy to use task manager
AddPackage xfce4-terminal # A modern terminal emulator primarily for the Xfce desktop environment
AddPackage xfce4-time-out-plugin # Take a break from your computer with this plugin for Xfce4
AddPackage xfce4-timer-plugin # Plugin to track time for the Xfce4 panel
AddPackage xfce4-verve-plugin # Command line plugin Xfce4 panel
AddPackage xfce4-wavelan-plugin # Plugin to monitor wifi connectivity for the Xfce4 panel
AddPackage xfce4-weather-plugin # A weather plugin for the Xfce4 panel
AddPackage xfce4-whiskermenu-plugin # Menu for Xfce4
AddPackage xfce4-xkb-plugin # Plugin to switch keyboard layouts for the Xfce4 panel
AddPackage xfconf # Flexible, easy-to-use configuration management system
AddPackage xfdesktop # A desktop manager for Xfce
AddPackage xfsprogs # XFS filesystem utilities
AddPackage xfwm4 # Xfce's window manager
AddPackage xfwm4-themes # A set of additional themes for the Xfce window manager
AddPackage xl2tpd # an open source implementation of the L2TP maintained by Xelerance Corporation
AddPackage xorg-bdftopcf # Convert X font from Bitmap Distribution Format to Portable Compiled Format
AddPackage xorg-font-util # X.Org font utilities
AddPackage xorg-iceauth # ICE authority file utility
AddPackage xorg-mkfontscale # Create an index of scalable font files for X
AddPackage xorg-server # Xorg X server
AddPackage xorg-server-xephyr # A nested X server that runs as an X application
AddPackage xorg-server-xvfb # Virtual framebuffer X server
AddPackage xorg-sessreg # Register X sessions in system utmp/utmpx databases
AddPackage xorg-setxkbmap # Set the keyboard using the X Keyboard Extension
AddPackage xorg-smproxy # Allows X applications that do not support X11R6 session management to participate in an X11R6 session
AddPackage xorg-x11perf # Simple X server performance benchmarker
AddPackage xorg-xauth # X.Org authorization settings program
AddPackage xorg-xbacklight # RandR-based backlight control application
AddPackage xorg-xcmsdb # Device Color Characterization utility for X Color Management System
AddPackage xorg-xcursorgen # Create an X cursor file from PNG images
AddPackage xorg-xdpyinfo # Display information utility for X
AddPackage xorg-xdriinfo # Query configuration information of DRI drivers
AddPackage xorg-xev # Print contents of X events
AddPackage xorg-xgamma # Alter a monitor's gamma correction
AddPackage xorg-xhost # Server access control program for X
AddPackage xorg-xinit # X.Org initialisation program
AddPackage xorg-xinput # Small commandline tool to configure devices
AddPackage xorg-xkbcomp # X Keyboard description compiler
AddPackage xorg-xkbevd # XKB event daemon
AddPackage xorg-xkbutils # XKB utility demos
AddPackage xorg-xkill # Kill a client by its X resource
AddPackage xorg-xlsatoms # List interned atoms defined on server
AddPackage xorg-xlsclients # List client applications running on a display
AddPackage xorg-xmodmap # Utility for modifying keymaps and button mappings
AddPackage xorg-xpr # Print an X window dump from xwd
AddPackage xorg-xprop # Property displayer for X
AddPackage xorg-xrandr # Primitive command line interface to RandR extension
AddPackage xorg-xrdb # X server resource database utility
AddPackage xorg-xrefresh # Refresh all or part of an X screen
AddPackage xorg-xset # User preference utility for X
AddPackage xorg-xsetroot # Classic X utility to set your root window background to a given pattern or color
AddPackage xorg-xvinfo # Prints out the capabilities of any video adaptors associated with the display that are accessible through the X-Video extension
AddPackage xorg-xwayland # run X clients under wayland
AddPackage xorg-xwd # X Window System image dumping utility
AddPackage xorg-xwininfo # Command-line utility to print information about windows on an X server
AddPackage xorg-xwud # X Window System image undumping utility
AddPackage xournalpp # Handwriting notetaking software with PDF annotation support
AddPackage xsecurelock # X11 screen lock utility with security in mind
AddPackage xyne-mirrorlist # Pacman mirrorlist for Xyne's repos.
AddPackage yay-bin # Yet another yogurt. Pacman wrapper and AUR helper written in go. Pre-compiled.
AddPackage yelp # Get help with GNOME
AddPackage youtube-dl # A command-line program to download videos from YouTube.com and a few more sites
AddPackage zafiro-icon-theme # A icon pack flat with light colors.
AddPackage zathura # Minimalistic document viewer
AddPackage zathura-pdf-poppler # Adds pdf support to zathura by using the poppler engine
AddPackage zenity # Display graphical dialog boxes from shell scripts
AddPackage zsh # A very advanced and programmable command interpreter (shell) for UNIX
AddPackage zsh-completions # Additional completion definitions for Zsh
