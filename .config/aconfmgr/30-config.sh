#!/usr/bin/bash

CopyFile /etc/default/cpupower
CopyFile /etc/powerpill/powerpill.json
CopyFile /etc/resolvconf.conf
CopyFile /etc/sddm.conf
CopyFile /etc/fstab
CopyFile /etc/xdg/reflector/reflector.conf
CopyFile /etc/systemd/logind.conf.d/10-pm.conf
CopyFile /etc/smartd.conf
CopyFile /usr/share/smartmontools/smartd_warning.d/smartdnotify
CopyFile /etc/sudoers.d/20-sudo-defaults 440
CopyFile /etc/unbound/unbound.conf
CopyFile /etc/systemd/logind.conf.d/10-pm.conf
CopyFile /etc/systemd/system/user@.service.d/local.conf
CopyFile /etc/xdg/reflector/reflector.conf
CopyFile /etc/hostname
CopyFile /etc/default/cpupower
CopyFile /etc/systemd/journald.conf
CopyFile /etc/ssh/sshd_config
CopyFile /etc/default/keyboard
CopyFile /etc/default/locale
CopyFile /etc/nanorc
CopyFile /etc/tlp.conf

# hooks
CopyFile /etc/pacman.d/hooks/refind.hook
CopyFile /etc/pacman.d/hooks/zsh-rehash.hook

# containers
CopyFile /etc/subgid
CopyFile /etc/subuid

# optimus
CopyFile /etc/optimus-manager/optimus-manager.conf


# udev-internet
CopyFile /etc/udev/rules.d/10-persistent-net.rules
CopyFile /etc/udev/rules.d/81-wifi-powersave.rules

# nvidia
CopyFile /usr/lib/udev/rules.d/80-nvidia-pm.rules
CopyFile /etc/modprobe.d/nvidia-power-management.conf
CopyFile /etc/modprobe.d/nvidia.conf

# arch
CopyFile /etc/mkinitcpio.conf
CopyFile /etc/pacman.conf

# vmware license
CopyFile /etc/vmware/license-ws-150-e1-201804
CopyFile /etc/vmware/license-ws-150-e2-201804
CopyFile /etc/vmware/license-ws-160-e5-202001

# generate locale after adding this
# https://wiki.archlinux.org/index.php/Locale#Generating_locales
CopyFile /etc/locale.gen

CopyFile /etc/nsswitch.conf

# (g)ufw
CopyFile /etc/gufw/Home.profile 600
CopyFile /etc/gufw/gufw.cfg 600
CopyFile /etc/ufw/ufw.conf

# hblock
CreateDir '/etc/hblock.d' '' chigozirim chigozirim
CopyFile /etc/hblock.d/allowlist '' chigozirim chigozirim
CopyFile /etc/hblock.d/denylist '' chigozirim chigozirim
CopyFile /etc/hblock.d/header '' chigozirim chigozirim
CopyFile /etc/systemd/system/hblock.service.d/override.conf

# networkmanager
CopyFile /etc/NetworkManager/conf.d/dhcp-client.conf
CopyFile /etc/NetworkManager/conf.d/dns-servers.conf
CopyFile /etc/NetworkManager/conf.d/dns.conf
CopyFile /etc/NetworkManager/conf.d/no-systemd-resolved.conf
CopyFile /etc/NetworkManager/conf.d/rc-manager.conf
CopyFile /etc/NetworkManager/conf.d/wifi-powersave-off.conf
CopyFile /etc/NetworkManager/conf.d/wifi_rand_mac.conf
CopyFile /etc/NetworkManager/system-connections/Canon_ij_Setup.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/eduroam.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/haniyke.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/SASKTEL293_5G.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/SHAW-57F380-5G.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/SHAW-57F380.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/SPL-Public.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/Starbucks\ WiFi.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/TP-LINK_Extender_5GHz.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/uofs-guest.nmconnection 600
CopyFile /etc/NetworkManager/system-connections/uofs-secure.nmconnection 600
