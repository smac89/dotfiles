#!/usr/bin/bash

IgnorePath /etc/X11/xorg.conf.save
IgnorePath /etc/adjtime
IgnorePath /etc/apparmor/parser.conf.pacsave
IgnorePath /etc/brlapi.key 640 '' brlapi
IgnorePath /etc/conf.d/fluidsynth
IgnorePath /etc/containers/registries.conf
IgnorePath /etc/containers/seccomp.json
IgnorePath /etc/containers/storage.conf
IgnorePath /etc/crypttab
IgnorePath /etc/default/thinkfan
IgnorePath /etc/docker/key.json 600
IgnorePath /etc/gconf/gconf.xml.defaults/%gconf-tree.xml
IgnorePath /etc/group
IgnorePath /etc/group-
IgnorePath /etc/gshadow
IgnorePath /etc/gshadow- 600
IgnorePath /etc/iproute2/rt_tables
IgnorePath /etc/ipsec.d/cacerts/NordVPN.der
IgnorePath /etc/ipsec.d/cacerts/NordVPN.pem
IgnorePath /etc/lsb-release
IgnorePath /etc/mkinitcpio.d/linux-lts.preset
IgnorePath /etc/mkinitcpio.d/linux.preset
IgnorePath /etc/ntpd.conf
CreateLink /etc/os-release ../usr/lib/os-release
IgnorePath /etc/pacman.d/mirrorlist
IgnorePath /etc/pacman.d/mirrorlist.bck
IgnorePath /etc/pam.d/system-login.old
IgnorePath /etc/passwd
IgnorePath /etc/passwd-
IgnorePath /etc/passwd.OLD
IgnorePath /etc/printcap
IgnorePath /etc/profile
IgnorePath /etc/resolv-secure.conf
IgnorePath /etc/resolv.conf.bak
IgnorePath /etc/resolv.conf.qomui.bak
IgnorePath /etc/sddm.conf.d/hidpi.conf
IgnorePath /etc/security/limits.d/10-gcr.conf
IgnorePath /etc/shadow
IgnorePath /etc/shadow- 600
IgnorePath /etc/shells
IgnorePath /etc/subgid-
IgnorePath /etc/subuid-
IgnorePath /etc/sudoers.d/10-installer 440
IgnorePath /etc/teamviewer/global.conf 600
IgnorePath /etc/thinkfan.conf
IgnorePath /etc/timeshift.json
IgnorePath /etc/vconsole.conf
IgnorePath /etc/xml/catalog
IgnorePath /opt/teamviewer/rolloutfile.tv13 600
IgnorePath /usr/lib/graphviz/config6
IgnorePath /usr/lib/os-release
IgnorePath /usr/lib/vlc/plugins/plugins.dat
IgnorePath /usr/share/glib-2.0/schemas/gschemas.compiled
IgnorePath /usr/share/info/dir
IgnorePath /usr/share/perl5/vendor_perl/XML/SAX/ParserDetails.ini
IgnorePath /usr/share/vim/vimfiles/doc/tags
IgnorePath /var/db/ntpd.drift
IgnorePath /var/spool/anacron/cron.daily 600
IgnorePath /var/spool/anacron/cron.monthly 600
IgnorePath /var/spool/anacron/cron.weekly 600
