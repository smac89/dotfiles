--[[
#=====================================================================================
#                               arcolinux
# Date    : package-date
# Author  : Erik Dubois at http://www.erikdubois.be
# Version : package-version
# License : Distributed under the terms of GNU GPL version 2 or later
# Documentation : http://erikdubois.be/
#======================================================================================
# CONKY
# For commands in conky.config section:
# http://conky.sourceforge.net/config_settings.html
#
# For commands in conky.text section:
# http://conky.sourceforge.net/variables.html
#
# A PDF with all variables is provided
#=====================================================================================
# FONTS
# To avoid copyright infringements you will have to download
# and install the fonts yourself sometimes.
#=====================================================================================
# GENERAL INFO ABOUT FONTS
# Go and look for a nice font on sites like http://www.dafont.com/
# Download and unzip - double click the font to install it (font-manager must be installed)
# No font-manager then put fonts in ~/.fonts
# Change the font name in the conky
# The name can be known with a command in the terminal: fc-list | grep "part of name"
# Change width and height of the conky according to font
# Reboot your system or fc-cache -fv in terminal
# Enjoy
#=====================================================================================
# FONTS FOR THIS CONKY
# http://www.dafont.com/style-bats.font
#======================================================================================

]]

conky.config = {

    --Various settings

    background = true, 								-- forked to background
    cpu_avg_samples = 2,							-- The number of samples to average for CPU monitoring.
    diskio_avg_samples = 10,						-- The number of samples to average for disk I/O monitoring.
    double_buffer = true,							-- Use the Xdbe extension? (eliminates flicker)
    if_up_strictness = 'link',						-- how strict if testing interface is up - up, link or address
    net_avg_samples = 2,							-- The number of samples to average for net data
    no_buffers = false,								-- Subtract (file system) buffers from used memory?
    temperature_unit = 'celsius',					-- fahrenheit or celsius
    text_buffer_size = 2048,						--[[ size of buffer for display of content of 
    													large variables - default 256 ]]
    update_interval = 1,							-- update interval
    update_interval_on_battery=1.5,					-- Update interval when running on battery power.
    imlib_cache_size = 0,                       	-- disable image cache to get a new spotify cover per song


    --Placement

    alignment = 'top_right',						--[[top_left,top_middle,top_right,bottom_left,
    													bottom_middle,bottom_right,middle_left,
    													middle_middle,middle_right,none]]
    --Arch Duoscreen
    --gap_x = -1910,
    gap_x = 10,										-- pixels between right or left border
    gap_y = 10,									--[[Gap, in pixels, between top or bottom border of 
    													screen, same as passing -y at command line, 
    													e.g. gap_y 10. For other position related stuff, see 
    													alignment'.]]
    minimum_height = 600,							-- minimum height of window
    minimum_width = 330,							-- minimum height of window
    maximum_width = 330,							-- maximum width of window

    --Graphical

    border_inner_margin = 5, 						-- margin between border and text
    border_outer_margin = 5, 						-- margin between border and edge of window
    border_width = 0, 								-- border width in pixels
    default_bar_width = 80,							-- default is 0 - full width
    default_bar_height = 10,						-- default is 6
    default_gauge_height = 25,						-- default is 25
    default_gauge_width =40,						-- default is 40
    default_graph_height = 40,						-- default is 25
    default_graph_width = 0,						-- default is 0 - full width
    default_shade_color = '#000000',				-- default shading colour
    default_outline_color = '#000000',				-- default outline colour
    draw_borders = false,							-- draw borders around text
    draw_graph_borders = true,						-- draw borders around graphs
    draw_shades = false,							-- draw shades
    draw_outline = false,							-- draw outline
    stippled_borders = 0,							-- dashing the border

    --Textual

    extra_newline = false,								-- extra newline at the end - for asesome's wiboxes
    format_human_readable = true,						-- KiB, MiB rather then number of bytes
    font = 'Roboto Mono for Powerline:Bold:size=10',	-- font for complete conky unless in code defined
    max_text_width = 0,									--[[0 will make sure line does not get broken if 
    														width too small]]
    max_user_text = 16384,								-- max text in conky default 16384
    override_utf8_locale = true,						-- force UTF8 requires xft
    short_units = false,								-- shorten units from KiB to k
    top_name_width = 21,								-- width for $top name value default 15
    top_name_verbose = false,							--[[If true, top name shows the full command line 
    														of  each  process - Default value is false.]]
    uppercase = false,									-- uppercase or not
    use_spacer = 'none',								-- adds spaces around certain objects to align - default none
    use_xft = true,										-- Use Xft (anti-aliased font and stuff)
    xftalpha = 1,										-- Alpha of Xft font. Must be a value at or between 1 and 0.

    --Windows
	--https://www.mankier.com/1/conky#Configuration_Settings	

    own_window = true,						-- create your own window to draw
   	own_window_argb_value = 0,				-- real transparency - composite manager required 0-255
   	own_window_argb_visual = true,			-- use ARGB - composite manager required
    own_window_class = 'Conky',				--[[Manually set the WM_CLASS name. 
    											Defaults to "Conky".]]
   	-- own_window_colour = '#ffffff',		-- set colour if own_window_transparent no
    own_window_hints = 'undecorated,'..
    				   'below,'..
    				   'sticky,'..
    				   'skip_taskbar,'..
    				   'skip_pager',		-- if own_window true - just hints - own_window_type sets it
    own_window_transparent = true,			-- if own_window_argb_visual is true sets background opacity 0%
    own_window_title = 'system_conky',		-- Manually set the window name. Defaults to "<hostname> - conky".
    own_window_type = 'dock',			-- if own_window true options are: normal/override/dock/desktop/panel


    --Colours

    default_color = '#D9DDE2',  				-- default color and border color
    color1 = '#FF0000',
    color2 = '#ECC465',
    color3 = '#cccccc',
    color4 = '#e6a522',
    color5 = '#ff6347',
    color6 = '#FFFFFF',

    --Signal Colours
    color7 = '#1F7411',  						--green
    color8 = '#FFA726',  						--orange
    color9 = '#F1544B',  						--firebrick

    --Lua
};

conky.text = [[
${color6}${voffset 1}${font Roboto:size=36}${alignc}${time %H}:${time %M}${font}${color}
${color6}${voffset 1}${font Roboto:size=12}${alignc}${time %A} ${time %B} ${time %e}, ${time %Y}${font}${color}

${color5}${font Roboto:bold:size=12}${voffset 1}S Y S T E M   ${hr 2}${font}${color}
${color2}${voffset 8}Hostname:${color} ${alignr}${nodename}
${color2}Distro:${color}${alignr}$sysname $kernel ${alignr}${execi 6000 lsb_release -a | grep 'Release'|awk {'print $2""$3""$4""$5'}}
${color2}Kernel:${color}${alignr}${exec uname} ${exec uname -r}
#${color2}Nvidia:${color}${alignr}${execp  nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}
${color2}Nvidia Driver:${color}${alignr}${execp  nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}  v${execi 60000 nvidia-smi | grep "Driver Version"| awk {'print $3'}}
${color2}Uptime:${color} ${alignr}${uptime}

${color5}${font Roboto:bold:size=12}P R O C E S S O R S  ${hr 2}${font}${color}

${color2}CPU Freq:${color} $alignr${freq}MHz
#${color2}CPU Temp:${color} $alignr${execi 10 sensors | grep 'Core 0' | awk {'print $3'}}
${color2}CPU Temp:${color} $alignr${hwmon coretemp temp 1}°C
${color2}History:${color} ${alignr}${cpugraph 8,100 ffffff F1544B -t -l}

${color4}${font StyleBats:size=20}A${font}${color}${offset 1}${voffset -30}
${color2}${offset 27}CPU Core 1:${color} ${alignr}${offset -10}${cpu cpu1}%${alignr}${cpubar cpu1}
${color2}${offset 27}CPU Core 2:${color} ${alignr}${offset -10}${cpu cpu2}%${alignr}${cpubar cpu2}
${color2}${offset 27}CPU Core 3:${color} ${alignr}${offset -10}${cpu cpu3}%${alignr}${cpubar cpu3}
${color2}${offset 27}CPU Core 4:${color} ${alignr}${offset -10}${cpu cpu4}%${alignr}${cpubar cpu4}
${color2}${offset 27}CPU Core 5:${color} ${alignr}${offset -10}${cpu cpu5}%${alignr}${cpubar cpu5}
${color2}${offset 27}CPU Core 6:${color} ${alignr}${offset -10}${cpu cpu6}%${alignr}${cpubar cpu6}
${color2}${offset 27}CPU Core 7:${color} ${alignr}${offset -10}${cpu cpu7}%${alignr}${cpubar cpu7}
${color2}${offset 27}CPU Core 8:${color} ${alignr}${offset -10}${cpu cpu8}%${alignr}${cpubar cpu8}
${color2}${offset 27}CPU Core 9:${color} ${alignr}${offset -10}${cpu cpu9}%${alignr}${cpubar cpu9}
${color2}${offset 27}CPU Core 10:${color} ${alignr}${offset -10}${cpu cpu10}%${alignr}${cpubar cpu10}
${color2}${offset 27}CPU Core 11:${color} ${alignr}${offset -10}${cpu cpu11}%${alignr}${cpubar cpu11}
${color2}${offset 27}CPU Core 12:${color} ${alignr}${offset -10}${cpu cpu12}%${alignr}${cpubar cpu12}

${color2}Top Processes${goto 250}cpu%${goto 300}mem%${color}${voffset 15}
${offset 27}1  -  ${top name 1}${alignr}${goto 235}${top cpu 1} ${goto 285}${top mem 1}
${offset 27}2  -  ${top name 2}${alignr}${goto 235}${top cpu 2} ${goto 285}${top mem 2}
${offset 27}3  -  ${top name 3}${alignr}${goto 235}${top cpu 3} ${goto 285}${top mem 3}
${offset 27}4  -  ${top name 4}${alignr}${goto 235}${top cpu 4} ${goto 285}${top mem 4}
${offset 27}5  -  ${top name 5}${alignr}${goto 235}${top cpu 5} ${goto 285}${top mem 5}

${color5}${font Roboto:bold:size=12}M E M O R Y   ${hr 2}${font}${color}

${color4}${font StyleBats:size=20}M${font}${color}${offset 8}${voffset -12}${color2}RAM: ${color}${alignr}${offset -10}${mem} / ${memmax}${alignr}${membar}
${color2}${offset 30}Swap:${color} ${alignr}${offset -10}${swap} / ${swapmax}${alignr}${swapbar}

#${color2}Top Processes${goto 222}cpu%${goto 274}mem%${color}
#${voffset 4}     1  -  ${top_mem name 1}${alignr}${goto 170} ${goto 222}${top_mem cpu 1} ${goto 274}${top_mem mem 1}
#     2  -  ${top_mem name 2}${alignr}${goto 170} ${goto 222}${top_mem cpu 2} ${goto 274}${top_mem mem 2}
#     3  -  ${top_mem name 3}${alignr}${goto 170} ${goto 222}${top_mem cpu 3} ${goto 274}${top_mem mem 3}
#     4  -  ${top_mem name 4}${alignr}${goto 170} ${goto 222}${top_mem cpu 4} ${goto 274}${top_mem mem 4}
#     5  -  ${top_mem name 5}${alignr}${goto 170} ${goto 222}${top_mem cpu 5} ${goto 274}${top_mem mem 5}

${color5}${font Roboto:bold:size=12}D R I V E S   ${hr 2}${font}${color}

${color4}${font StyleBats:size=20}P${font}${color}${offset 8}${voffset -12}${color2}Root:${color} ${alignr}${offset -10}${fs_used /} / ${fs_size /}${alignr}${fs_bar}
${offset 30}${color2}I/O Read:${color} ${alignr}${offset -10}${diskio_read /dev/sda4}${alignr}${diskiograph_read /dev/sda4 8,100}
${offset 30}${color2}I/O Write:${color} ${alignr}${offset -10}${diskio_write /dev/sda4}${alignr}${diskiograph_write /dev/sda4 8,100}

${color4}${font StyleBats:size=20}G${font}${color}${offset 8}${voffset -12}${color2}Home:${color} ${alignr}${offset -10}${fs_used /home} / ${fs_size /home}${alignr}${fs_bar /home}
${offset 30}${color2}I/O Read:${color} ${alignr}${offset -10}${diskio_read /dev/sda5}${alignr}${diskiograph_read /dev/sda5 8,100}
${offset 30}${color2}I/O Write:${color} ${alignr}${offset -10}${diskio_write /dev/sda5}${alignr}${diskiograph_write /dev/sda5 8,100}

${color5}${font Roboto:bold:size=12}N E T W O R K   ${hr 2}${font}${color}
${voffset -30}
${if_up net1}
${color4}${font StyleBats:size=20}5${font}${color}${offset 8}
${voffset -30}${color2}${offset 34}Upload:${color}${alignr}${offset -10$}${upspeed net1}${alignr}${upspeedgraph net1 8,100}
${color2}${offset 34}Download:${color}${alignr}${offset -10$}${downspeed net1}${alignr}${downspeedgraph net1 8,100}
${else}
${if_up wifi0}
${color4}${font StyleBats:size=20}5${font}${color}${offset 8}
${voffset -30}${color2}${offset 34}Upload:${color}${alignr}${offset -10$}${upspeed wifi0}${alignr}${upspeedgraph wifi0 8,100}
${color2}${offset 34}Download:${color}${alignr}${offset -10$}${downspeed wifi0}${alignr}${downspeedgraph wifi0 8,100}
${endif}
${endif}
#${font Roboto:size=10}N V I D I A   ${hr 2}${font}
#${font Roboto:size=10,weight:bold}${color5}${execp  nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}${font}
#${font StyleBats:size=20}u${font}${offset 8}${voffset -12}GPU Temp ${alignr}${execi 60 nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader} °C
#${offset 30}Fan Speed ${alignr}${execi 60 nvidia-settings -q [fan:0]/GPUCurrentFanSpeed -t} %
#${offset 30}GPU Clock ${alignr}${execi 60 nvidia-settings -q GPUCurrentClockFreqs | grep -m 1 Attribute | awk '{print $4}' | sed -e 's/\.//' | cut -d, -f1} MHz
#${offset 30}Mem Clock ${alignr}${execi 86400 nvidia-settings -q all| grep -m 1 GPUCurrentProcessorClockFreqs | awk '{print $4}' | sed 's/.$//'} MHz
#${offset 30}Mem Used ${alignr}${execi 60 nvidia-settings -q [gpu:0]/UsedDedicatedGPUMemory -t} / ${exec nvidia-settings -q [gpu:0]/TotalDedicatedGPUMemory -t} MiB0
]];
