# Set the default Less options.
# Mouse-wheel scrolling has been disabled by -X (disable screen clearing).
# Remove -X and -F (exit if the content fits on one screen) to enable it.
export LESS='-g -i -M -N -S -R -w -z-4'

# https://github.com/dandavison/delta/issues/582
# export DELTA_PAGER="less ${LESS}"

# These are good to be left here because it works for
# every subsequent shell started by the user, including non-zsh
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"

# Ensure path arrays do not contain duplicates.
typeset -U path PATH cdpath CDPATH fpath FPATH manpath MANPATH

# Android
export ANDROID_SDK_ROOT="$HOME/Libraries/android/sdk"
prepend_path 'PATH' "$ANDROID_SDK_ROOT/platform-tools"

# Flutter
export FLUTTER_ROOT="$HOME/Libraries/flutter"
prepend_path 'PATH' "$FLUTTER_ROOT/bin"

# for python user-installed scripts
[ -d "${HOME}/.local/bin" ] && prepend_path 'PATH' "$HOME/.local/bin"

# rubygems
export GEM_HOME="$XDG_DATA_HOME/gem"

# https://docs.gradle.org/current/userguide/build_environment.html#sec:gradle_environment_variables
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"

# virtualenvwrapper
export WORKON_HOME="$XDG_DATA_HOME/virtualenvs"
# https://virtualenv.pypa.io/en/latest/user_guide.html#seeders
export VIRTUALENV_OVERRIDE_APP_DATA="$XDG_CACHE_HOME/virtualenv"

# https://wiki.archlinux.org/index.php/Jupyter#Installation
export JUPYTERLAB_DIR="$XDG_DATA_HOME/jupyter/lab"

# https://jupyter.readthedocs.io/en/latest/use/jupyter-directories.html#configuration-files
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME/jupyter"

# https://docs.julialang.org/en/v1/manual/environment-variables/
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"

# https://ipython.org/ipython-doc/stable/config/intro.html#the-ipython-directory
export IPYTHONDIR="$XDG_CONFIG_HOME/ipython"

# https://docs.pylint.org/en/1.6.0/faq.html#where-is-the-persistent-data-stored-to-compare-between-successive-runs
export PYLINTHOME="$XDG_DATA_HOME/pylint"

# https://docs.conan.io/en/latest/reference/env_vars.html#conan-user-home
export CONAN_USER_HOME="$XDG_DATA_HOME/conan"

# https://github.com/dotnet/sdk/issues/9514
export DOTNET_CLI_HOME="$XDG_DATA_HOME/dotnet"

# https://docs.microsoft.com/en-us/nuget/consume-packages/managing-the-global-packages-and-cache-folders
export NUGET_PACKAGES="$XDG_DATA_HOME/NuGet/packages"
export NUGET_PLUGINS_CACHE_PATH="$XDG_CACHE_HOME/NuGet/plugins-cache"
export NUGET_HTTP_CACHE_PATH="$XDG_CACHE_HOME/NuGet/v3-cache"

# https://doc.rust-lang.org/cargo/guide/cargo-home.html#cargo-home
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# https://wiki.archlinux.org/index.php/XDG_Base_Directory
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

# https://linux.die.net/man/1/rlwrap
export RLWRAP_HOME="$XDG_CACHE_HOME/rlwrap"

# https://nodejs.org/api/repl.html#repl_environment_variable_options
export NODE_REPL_HISTORY="$XDG_CACHE_HOME/node_repl_history"
export NODE_REPL_HISTORY_SIZE="10000"

# https://wiki.archlinux.org/index.php/GnuPG#Configuration
export GNUPGHOME="$XDG_DATA_HOME/gnupg"

# See ~/.zshenv for the definitions
# unfunction prepend_path init_autocomplete lazy_load __fix_bin_env &> /dev/null
