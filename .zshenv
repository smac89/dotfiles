# https://unix.stackexchange.com/a/453339/44793
TIMEFMT=$'=> \e[1m%J\e[0m <=\n\n real\t%E\n user\t%U\n sys\t%S\n cpu\t%P'

# prepends the given path(s) to the supplied PATH variable
# ex. prepend_path 'PATH' "$(go env GOPATH)/bin"
prepend_path() {
	# The number of args has to be 2
	# and the first arg cannot be empty
	if [[ "${#@}" -lt 2 || -z "$1" ]]; then
		return
	fi

    function {
        # (Ps.:.)1: $1 is expanded and split on :
        # ##: Removes leading :
        local p
        local path=("${(Ps.:.)1##:}")
        # %%[[:space:]/] also removes trailing space or slash
        for p in "${(s.:.)${2%%[[:space:]/]}##[[:space:]]}"; do
            # check for symbolic link and expand it
            if builtin test -L "$p"; then
                # :P Behaves similar to realpath(3)
                p="${p:P}"
            fi
            path=("$p" "${(@)path}")
        done
        # (j/:/)path: $path elements are joined with : and duplicates removed (@u)
        print -z "${(j/:/)${(@u)path}}"
    } "${@}" && read -z "$1"
}

# creates shim functions
shim_funcs() {
	local shim
	foreach shim ( $("$1" shims --short) ) {
		eval "$shim() { $1 exec $shim \${@} }"
	}
}

# manwidth
export MANWIDTH="$(($(tput cols) - 10))"
