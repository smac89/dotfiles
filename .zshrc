# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=7

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="dd.mm.yyyy"
HISTSIZE=100000
SAVEHIST=$HISTSIZE
HISTFILE="$XDG_CACHE_HOME/zsh_history"

# Do not print the directory stack after pushd or popd. 
setopt pushdsilent

# Have pushd with no arguments act like ‘pushd $HOME’.
setopt pushdtohome

source $ZSH/oh-my-zsh.sh

# if not running interactively, no need to continue
[[ ! -o interactive ]] && return

# https://github.com/zdharma/zinit#customizing-paths
declare -A ZINIT
ZINIT[ZCOMPDUMP_PATH]="$ZSH_COMPDUMP"
ZINIT[HOME_DIR]="${XDG_DATA_HOME}/zinit"
ZINIT[OPTIMIZE_OUT_DISK_ACCESSES]=1

# prompt
neofetch | lolcat
eval "$(starship init zsh)"

# https://wiki.archlinux.org/index.php/zsh#Alternative_on-demand_rehash_using_SIGUSR1
trap 'rehash' USR1

module_path+=( "$ZINIT[HOME_DIR]/bin/zmodules/Src" )
zmodload zdharma/zplugin

source "$ZINIT[HOME_DIR]/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

zi light zinit-zsh/z-a-submods

############################# PLUGINS #############################
# https://zdharma.org/zinit/wiki/INTRODUCTION/#listing_completions

# pyenv, rbenv, sdkman
zi wait='2a' lucid load depth='1' \
			atpull='%atclone' for \
    atclone='PYENV_ROOT="$PWD" PATH="$PWD/shims:${PATH}" \
    	bin/pyenv init - > pyenv.zsh' \
    	submods='pyenv/pyenv-pip-migrate -> plugins/pyenv-pip-migrate' \
    	submods='pyenv/pyenv-virtualenvwrapper -> plugins/pyenv-virtualenvwrapper' \
    	atinit='export PYENV_ROOT="$PWD";
    		export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"' \
    	atload='shim_funcs pyenv' \
    	pick='bin/pyenv' as='command' nocompile='!' src="pyenv.zsh" \
    pyenv/pyenv \
    \
    atclone='RBENV_ROOT="$PWD" \
    	bin/rbenv init - | sed -E --sandbox "/^export\s+PATH=.*$/{H;d}" > rbenv.zsh' \
    	submods='rbenv/ruby-build -> plugins/ruby-build' \
    	atinit='export RBENV_ROOT="$PWD"' \
    	atload='shim_funcs rbenv' \
    	pick='bin/rbenv' as='command' src='rbenv.zsh' \
    rbenv/rbenv \
    \
    atclone='export SDKMAN_DIR="${XDG_DATA_HOME}/sdkman";
    	export ZSH_SDKMAN_DIR="$PWD"' \
    	atinit='export ZSH_SDKMAN_DIR="$PWD";
        	export SDKMAN_DIR="${XDG_DATA_HOME}/sdkman"' \
        if='[[ -f "${XDG_DATA_HOME}/sdkman/bin/sdkman-init.sh" ]]' \
    matthieusb/zsh-sdkman

# nvm
zi wait='3a' lucid load depth='1' for \
    submods='nvm-sh/nvm -> nvm' \
    atinit='export NVM_DIR="$PWD/nvm";
        export NVM_COLORS="cmgRY";
        export NVM_COMPLETION=false;
        export NVM_AUTO_USE=false;
        export NVM_LAZY_LOAD=false;
        export NVM_LAZY_LOAD_EXTRA_COMMANDS=()' \
    lukechilds/zsh-nvm

# man, git, fast-syntax-highlighting, zsh-completions, pip, pipenv
zi wait='1b' lucid for \
	light-mode \
		OMZ::plugins/colored-man-pages \
		OMZ::plugins/git/git.plugin.zsh \
	light-mode \
		zdharma/fast-syntax-highlighting \
	blockf light-mode \
		zsh-users/zsh-completions \
	atinit='export PIPENV_SHELL_FANCY=true' \
    	OMZ::plugins/pipenv \
    light-mode id-as="pip/completion" \
    	atclone="command pip completion --zsh > pip" \
    	atpull="%atclone" src='pip' zdharma/null

# git-fast, docker, docker-compose
zi wait='2b' as="completion" lucid for \
    svn OMZ::plugins/gitfast \
    svn OMZ::plugins/docker \
    svn OMZ::plugins/docker-compose \
    light-mode gradle/gradle-completion

# zsh-autosuggestions
zi wait lucid atinit='ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20;
    ZSH_AUTOSUGGEST_STRATEGY=(history);
    ZSH_AUTOSUGGEST_USE_ASYNC=1' \
    atload='!_zsh_autosuggest_start' light-mode for \
    zsh-users/zsh-autosuggestions

# For GNU ls (the binaries can be gls, gdircolors, e.g. on OS X when installing the
# coreutils package from Homebrew; you can also use https://github.com/ogham/exa)
zi wait atclone"dircolors --sh LS_COLORS > c.zsh" \
    atpull'%atclone' pick"c.zsh" nocompile'!' lucid for \
    light-mode trapd00r/LS_COLORS

########################### PLUGINS END ###########################

# init autocomplete
function {
    # http://zsh.sourceforge.net/Doc/Release/Options.html#Scripts-and-Functions
    setopt LOCAL_OPTIONS extendedglob
    # http://zsh.sourceforge.net/Doc/Release/Expansion.html#Glob-Qualifiers
    # http://zsh.sourceforge.net/Doc/Release/Conditional-Expressions.html#Conditional-Expressions
    if [[ ! -e "${ZSH_COMPDUMP}" || -n ${ZSH_COMPDUMP}(#qNY1.mh+24) ]]; then
        # http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Use-of-compinit
        # zi cclear
        # rm -f $ZINIT[HOME_DIR]/completions/*
        zi compinit > /dev/null
        zi cdreplay -q
    fi
} &!

# https://wiki.archlinux.org/index.php/Zsh#The_%22command_not_found%22_handler
source /usr/share/doc/pkgfile/command-not-found.zsh

[ -d "${HOME}/.bin" ] && prepend_path 'PATH' "$HOME/.bin"

# See ~/.zshenv for the definitions
unfunction prepend_path init_autocomplete &> /dev/null

# Adapted from:
# https://gist.github.com/resilar/ade1e0311755e7e0a402cbecc836f486
case "$TERM" in (Eterm*|alacritty*|aterm*|gnome*|konsole*|kterm*|putty*|rxvt*|screen*|tmux*|xterm*)
    function show_cmd_preexec() {
        local CMD="${(j:\n:)${(f)2}}"
        print -n "\e]0;🚀 ${(j: :q)CMD}\a"
    }
    preexec_functions+=(show_cmd_preexec)

    # https://wiki.archlinux.org/title/zsh#Resetting_the_terminal_with_escape_sequences
    function reset_broken_terminal() {
        printf '%b' '\e[0m\e(B\e)0\017\e[?5l\e7\e[0;0r\e8'
    }

    function set_cmd_status() {
        # https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html#Conditional-Substrings-in-Prompts
        print -Pn "\e]0; %(?.✔️.🤒 $history[$((HISTCMD - 1))]) \a"
    }
    precmd_functions+=(reset_broken_terminal set_cmd_status)
    ;;
esac

# aliases
[ -f "$HOME/.aliases" ] && source ~/.aliases

# http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Completion-System-Configuration
# describe options in full
zstyle ':completion:*:options'                      description 'yes'

# on processes completion complete all user processes
zstyle ':completion:*:processes'                    command 'ps -au$USER'
# provide verbose completion information
zstyle ':completion:*'                              verbose true
# Turn off hosts completion
zstyle ':completion:*:hosts'                        hosts
# define files to ignore for zcompile
zstyle ':completion:*:*:zcompile:*'                 ignored-patterns '(*~|*.zwc)'
zstyle ':completion:correct:'                       prompt 'correct to: %e'
# Ignore completion functions for commands you don't have:
zstyle ':completion::(^approximate*):*:functions'   ignored-patterns '_*'
# Provide more processes in completion of programs like killall:
zstyle ':completion:*:processes-names'              command 'ps c -u ${USER} -o comm --no-headers | uniq'
# complete manual by their section
zstyle ':completion:*:manuals'                      separate-sections true
zstyle ':completion:*:manuals.*'                    insert-sections   true
zstyle ':completion:*:man:*'                        menu yes select

# Fix slow pastes due to syntax-highlighting
# https://github.com/zsh-users/zsh-syntax-highlighting/issues/295#issuecomment-214581607
zstyle ':bracketed-paste-magic'                     active-widgets '.self-*'

# set descriptions format to enable group support
# zstyle ':completion:*:descriptions' format '[%d]'
# set list-colors to enable filename colorizing
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
